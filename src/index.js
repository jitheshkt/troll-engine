import React from 'react';
import ReactDOM from 'react-dom';
import {
Route,
Switch,
HashRouter
} from 'react-router-dom';
import Home from './Pages/Home';
import Result from './Pages/Result';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render((
<HashRouter basename={'/memes'}>
    <Switch>
        <Route exact path='/' component={Home}/>
        <Route path='/result/:searchTerm?' component={Result}/>
    </Switch>
</HashRouter>
), document.getElementById('root'));
registerServiceWorker();
