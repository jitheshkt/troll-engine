import React, { Component } from 'react';
import SearchField from '../Components/SearchField';
import RandomTags from '../Components/RandomTags';


class Home extends Component {
    render() {
        return (
            <section className="main-content main-bg">
            <div className="content-outer">
            <SearchField />	
            <RandomTags />	
            </div>
            </section>
        );
    }
}

export default Home;