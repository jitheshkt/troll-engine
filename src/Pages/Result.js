import React from 'react';
import {fetchSearchResults} from '../Services/Api';

class Result extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword : this.props.match.params.searchTerm,
            apiResponse : [],
            loading : true,
            ref: 'gallery'
        };
    }
    componentDidMount() {
        var that = this;
        if(this.state.keyword) {
        fetchSearchResults(this.state.keyword)
        .then(function(response) {
            if(!response)
            {
                let finalHtml = <div className="loading-wrapper">
                                <h3>No memes found!</h3>
                                </div>;
                that.setState({apiResponse:finalHtml});
                that.setState({loading:false});
            }
            else
            {
                let searchResult = response.payload.map((pic) => {
                    return (
                        <li key={pic.id}>
                        <div className="result-box">
                            <img src={pic.imageurl} alt={pic.id} className="troll-image"/>
                            <div className="bar-btm-wrap"> <span className="dwnld-count">0 downloads</span> <span className="act-icons-wrap"><a href={pic.imageurl} download><i className="fa fa-download" aria-hidden="true"></i></a> </span> </div>
                        </div>
                        </li>
                    );
                });
                let finalHtml = <section className="container">
                                <div className="serach-result-wrap">
                                    <ul className="result-row">
                                        {searchResult}
                                    </ul>
                                </div>
                            </section>;
                that.setState({apiResponse:finalHtml});
                that.setState({loading:false});
            }
        }).catch((error) => {
            console.log(error);
        });
    } else {
        this.props.history.push('/');
    }
    }

    render() {
            if(this.state.loading) {
                return(
                    <div className="loading-wrapper">
                        <h1>Loading...</h1>
                    </div>
                );
            } else {
                return(
                    this.state.apiResponse
                );
            }
    }
}

export default Result;