import React from 'react';
import {Link} from 'react-router-dom';
import {fetchRandomTags} from '../Services/Api';

class RandomTags extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tags : []
        };
    }
    componentDidMount() {
        var that = this;
        const TAG_URL = (tag) => `/result/${tag}/`;
        fetchRandomTags().then(function(response){
            let tags  = response.payload.map((tag, index) => {
                return (
                    <li key={index}>
                        <Link to={TAG_URL(tag)}>{tag}</Link>
                    </li>
                );
            });
        that.setState({tags : tags});
        });
    }
    render() {
        return (
            <ul className="suggessions">
            {this.state.tags}
            </ul>
        );
    }
}

export default RandomTags;