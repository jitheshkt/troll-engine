import React, { Component } from 'react';
import {withRouter} from 'react-router-dom';

class SearchField extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword : ''
        };
        this.textInputChangeEvent = this.textInputChangeEvent.bind(this);
        this.doSearch = this.doSearch.bind(this);
    }
    doSearch() {
        console.log(this.state.keyword);
        this.props.history.push('/result/'+this.state.keyword);
    }
    textInputChangeEvent(event) {
        this.setState({keyword : event.target.value});
    }
    render() {
        return (
            <form className="search-outer">
            <input type="text" className="form-control input-text" onChange={this.textInputChangeEvent}/>	
            <input type="submit" className="input-submit" onClick={this.doSearch}/>
            </form>
        );
    }
}
  
export default withRouter(SearchField);