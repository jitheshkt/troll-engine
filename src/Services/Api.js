import axios from 'axios';

const BASEURL = 'http://45.77.201.8:3000/api/';

export function fetchSearchResults(keyword) {
    var encodedURI  = window.encodeURI(BASEURL+'search?s='+keyword);
    return axios.get(encodedURI)
    .then(function(response) {
        return response.data;
    });
}

export function fetchRandomTags() {
    var encodedURI  = window.encodeURI(BASEURL+'tags');
    return axios.get(encodedURI)
    .then(function(response) {
        return response.data;
    });
}